﻿using HR_Manager.Helpers;
using HR_Manager.ViewModels;
using System.Windows.Controls;
using System.Windows.Input;

namespace HR_Manager.pages
{
    /// <summary>
    /// Interaction logic for NewCandidatePage.xaml
    /// </summary>
    public partial class NewCandidatePage : Page
    {
        public NewCandidatePage()
        {
            InitializeComponent();
            DataContext = new NewCandidateViewModel();
        }

        public void NumericValidation(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !ValidationMethods.IsNoNPositiveNumericText(e.Text);
        }

    }
}

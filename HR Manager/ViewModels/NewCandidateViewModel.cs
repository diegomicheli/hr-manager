﻿using HR_Manager.Helpers;
using HR_Manager.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HR_Manager.ViewModels
{
    public class NewCandidateViewModel : BaseViewModel<Candidate>
    {
        public string Name { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Document { get; set; }
        public decimal AspiringSalary { get; set; }
        public string RecommendedBy { get; set; }
        public Opening Opening { get; set; }
        public List<Opening> AvailableOpenings { get; set; }

        public NewCandidateViewModel()
        {
            AvailableOpenings = GetAvailableOpenings();
        }

        private List<Opening> GetAvailableOpenings()
        {
            using (var context = new HRManagerModelsContainer())
            {
                return context.Openings.Where(opening => opening.State != State.CLOSED).ToList();
            }
        }

        protected override Candidate GetSubjectEntity()
        {
            return new Candidate {
                Name = Name,
                LastName = LastName,
                Email = Email,
                Document = Document,
                AspiringSalary = AspiringSalary,
                RecommendedBy = RecommendedBy,
                Opening = Opening,
                ApplicationStartDate = DateTime.Now
            };
        }

    }
}

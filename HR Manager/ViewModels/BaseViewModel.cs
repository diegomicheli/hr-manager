﻿using HR_Manager.Helpers;
using HR_Manager.Models;
using System.Linq;
using System.Windows.Input;

namespace HR_Manager.ViewModels
{
    public abstract class BaseViewModel<T> where T : class
    {
        public bool isValidationError { get; set; }
        private ICommand _saveCommand;

        public ICommand SaveCommand
        {
            get
            {
                return _saveCommand ?? (_saveCommand = new CommandHandler(() => Save(), !isValidationError));
            }
        }

        public void Save()
        {
            var subjectEntity = GetSubjectEntity();

            using (var context = new HRManagerModelsContainer())
            {
                var isExisting = context.Set<T>().Local.Any(e => e == subjectEntity);

                if (!isExisting)
                {
                    context.Entry<T>(subjectEntity).State = System.Data.Entity.EntityState.Added;
                    context.SaveChanges();
                }
                else
                {
                    context.Entry<T>(subjectEntity).State = System.Data.Entity.EntityState.Modified;
                    context.SaveChanges();
                }

            }
        }

        protected abstract T GetSubjectEntity();


    }
}

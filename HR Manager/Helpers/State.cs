﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR_Manager.Helpers
{
    public class State
    {
        public const string OPEN = "OPEN";
        public const string CLOSED = "CLOSED";
    }
}

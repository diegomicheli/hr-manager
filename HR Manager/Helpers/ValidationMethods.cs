﻿using System.Text.RegularExpressions;

namespace HR_Manager.Helpers
{
    public class ValidationMethods
    {
        public static bool IsNoNPositiveNumericText(string text)
        {
            Regex regex = new Regex("[^0-9.-]+");

            if(!string.IsNullOrEmpty(text) && !regex.IsMatch(text))
            {
                return decimal.Parse(text) > 0;
            }

            return false;
        }

        public static bool IsValidEmailAddress(string text)
        {
            Regex regex = new Regex(@"^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$");
            return regex.IsMatch(text);
        }
    }
}

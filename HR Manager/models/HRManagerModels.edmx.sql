
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 02/18/2018 00:16:49
-- Generated from EDMX file: C:\Users\Diego\source\repos\HR Manager\HR Manager\Models\HRManagerModels.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [HRManagerDatabase];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_OpeningCandidate]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Users_Candidate] DROP CONSTRAINT [FK_OpeningCandidate];
GO
IF OBJECT_ID(N'[dbo].[FK_CandidateSkill]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Skills] DROP CONSTRAINT [FK_CandidateSkill];
GO
IF OBJECT_ID(N'[dbo].[FK_CandidateJobExpirience]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[JobExpiriences] DROP CONSTRAINT [FK_CandidateJobExpirience];
GO
IF OBJECT_ID(N'[dbo].[FK_CandidateLanguage]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Languages] DROP CONSTRAINT [FK_CandidateLanguage];
GO
IF OBJECT_ID(N'[dbo].[FK_CandidateTraining]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Trainings] DROP CONSTRAINT [FK_CandidateTraining];
GO
IF OBJECT_ID(N'[dbo].[FK_CandidateEmployee]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Users_Candidate] DROP CONSTRAINT [FK_CandidateEmployee];
GO
IF OBJECT_ID(N'[dbo].[FK_Candidate_inherits_User]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Users_Candidate] DROP CONSTRAINT [FK_Candidate_inherits_User];
GO
IF OBJECT_ID(N'[dbo].[FK_Employee_inherits_User]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Users_Employee] DROP CONSTRAINT [FK_Employee_inherits_User];
GO
IF OBJECT_ID(N'[dbo].[FK_HRManager_inherits_User]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Users_HRManager] DROP CONSTRAINT [FK_HRManager_inherits_User];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Users]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Users];
GO
IF OBJECT_ID(N'[dbo].[Openings]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Openings];
GO
IF OBJECT_ID(N'[dbo].[Skills]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Skills];
GO
IF OBJECT_ID(N'[dbo].[JobExpiriences]', 'U') IS NOT NULL
    DROP TABLE [dbo].[JobExpiriences];
GO
IF OBJECT_ID(N'[dbo].[Trainings]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Trainings];
GO
IF OBJECT_ID(N'[dbo].[Languages]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Languages];
GO
IF OBJECT_ID(N'[dbo].[Users_Candidate]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Users_Candidate];
GO
IF OBJECT_ID(N'[dbo].[Users_Employee]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Users_Employee];
GO
IF OBJECT_ID(N'[dbo].[Users_HRManager]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Users_HRManager];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Users'
CREATE TABLE [dbo].[Users] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [LastName] nvarchar(max)  NOT NULL,
    [Email] nvarchar(max)  NOT NULL,
    [Document] nvarchar(max)  NOT NULL,
    [Password] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Openings'
CREATE TABLE [dbo].[Openings] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Title] nvarchar(max)  NOT NULL,
    [RiskLevel] nvarchar(max)  NOT NULL,
    [MinimunSalary] decimal(18,0)  NOT NULL,
    [MaximunSalary] decimal(18,0)  NOT NULL,
    [State] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Skills'
CREATE TABLE [dbo].[Skills] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Description] nvarchar(max)  NOT NULL,
    [State] bit  NOT NULL,
    [CandidateId] int  NOT NULL
);
GO

-- Creating table 'JobExpiriences'
CREATE TABLE [dbo].[JobExpiriences] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Company] nvarchar(max)  NOT NULL,
    [Description] nvarchar(max)  NOT NULL,
    [StartDate] datetime  NOT NULL,
    [EndDate] datetime  NOT NULL,
    [CandidateId] int  NOT NULL,
    [Salary] decimal(18,0)  NOT NULL
);
GO

-- Creating table 'Trainings'
CREATE TABLE [dbo].[Trainings] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Description] nvarchar(max)  NOT NULL,
    [Level] nvarchar(max)  NOT NULL,
    [StartDate] datetime  NOT NULL,
    [EndDate] datetime  NOT NULL,
    [Institution] nvarchar(max)  NOT NULL,
    [CandidateId] int  NOT NULL
);
GO

-- Creating table 'Languages'
CREATE TABLE [dbo].[Languages] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Title] nvarchar(max)  NOT NULL,
    [State] nvarchar(max)  NOT NULL,
    [CandidateId] int  NOT NULL
);
GO

-- Creating table 'Users_Candidate'
CREATE TABLE [dbo].[Users_Candidate] (
    [OpeningId] int  NOT NULL,
    [ApplicationStartDate] datetime  NOT NULL,
    [AspiringSalary] decimal(18,0)  NOT NULL,
    [RecommendedBy] nvarchar(max)  NOT NULL,
    [Id] int  NOT NULL,
    [Employee_Id] int  NOT NULL
);
GO

-- Creating table 'Users_Employee'
CREATE TABLE [dbo].[Users_Employee] (
    [Position] nvarchar(max)  NOT NULL,
    [Salary] nvarchar(max)  NOT NULL,
    [StartDate] datetime  NOT NULL,
    [State] nvarchar(max)  NOT NULL,
    [Id] int  NOT NULL
);
GO

-- Creating table 'Users_HRManager'
CREATE TABLE [dbo].[Users_HRManager] (
    [Id] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'Users'
ALTER TABLE [dbo].[Users]
ADD CONSTRAINT [PK_Users]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Openings'
ALTER TABLE [dbo].[Openings]
ADD CONSTRAINT [PK_Openings]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Skills'
ALTER TABLE [dbo].[Skills]
ADD CONSTRAINT [PK_Skills]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'JobExpiriences'
ALTER TABLE [dbo].[JobExpiriences]
ADD CONSTRAINT [PK_JobExpiriences]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Trainings'
ALTER TABLE [dbo].[Trainings]
ADD CONSTRAINT [PK_Trainings]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Languages'
ALTER TABLE [dbo].[Languages]
ADD CONSTRAINT [PK_Languages]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Users_Candidate'
ALTER TABLE [dbo].[Users_Candidate]
ADD CONSTRAINT [PK_Users_Candidate]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Users_Employee'
ALTER TABLE [dbo].[Users_Employee]
ADD CONSTRAINT [PK_Users_Employee]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Users_HRManager'
ALTER TABLE [dbo].[Users_HRManager]
ADD CONSTRAINT [PK_Users_HRManager]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [OpeningId] in table 'Users_Candidate'
ALTER TABLE [dbo].[Users_Candidate]
ADD CONSTRAINT [FK_OpeningCandidate]
    FOREIGN KEY ([OpeningId])
    REFERENCES [dbo].[Openings]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_OpeningCandidate'
CREATE INDEX [IX_FK_OpeningCandidate]
ON [dbo].[Users_Candidate]
    ([OpeningId]);
GO

-- Creating foreign key on [CandidateId] in table 'Skills'
ALTER TABLE [dbo].[Skills]
ADD CONSTRAINT [FK_CandidateSkill]
    FOREIGN KEY ([CandidateId])
    REFERENCES [dbo].[Users_Candidate]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CandidateSkill'
CREATE INDEX [IX_FK_CandidateSkill]
ON [dbo].[Skills]
    ([CandidateId]);
GO

-- Creating foreign key on [CandidateId] in table 'JobExpiriences'
ALTER TABLE [dbo].[JobExpiriences]
ADD CONSTRAINT [FK_CandidateJobExpirience]
    FOREIGN KEY ([CandidateId])
    REFERENCES [dbo].[Users_Candidate]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CandidateJobExpirience'
CREATE INDEX [IX_FK_CandidateJobExpirience]
ON [dbo].[JobExpiriences]
    ([CandidateId]);
GO

-- Creating foreign key on [CandidateId] in table 'Languages'
ALTER TABLE [dbo].[Languages]
ADD CONSTRAINT [FK_CandidateLanguage]
    FOREIGN KEY ([CandidateId])
    REFERENCES [dbo].[Users_Candidate]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CandidateLanguage'
CREATE INDEX [IX_FK_CandidateLanguage]
ON [dbo].[Languages]
    ([CandidateId]);
GO

-- Creating foreign key on [CandidateId] in table 'Trainings'
ALTER TABLE [dbo].[Trainings]
ADD CONSTRAINT [FK_CandidateTraining]
    FOREIGN KEY ([CandidateId])
    REFERENCES [dbo].[Users_Candidate]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CandidateTraining'
CREATE INDEX [IX_FK_CandidateTraining]
ON [dbo].[Trainings]
    ([CandidateId]);
GO

-- Creating foreign key on [Employee_Id] in table 'Users_Candidate'
ALTER TABLE [dbo].[Users_Candidate]
ADD CONSTRAINT [FK_CandidateEmployee]
    FOREIGN KEY ([Employee_Id])
    REFERENCES [dbo].[Users_Employee]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CandidateEmployee'
CREATE INDEX [IX_FK_CandidateEmployee]
ON [dbo].[Users_Candidate]
    ([Employee_Id]);
GO

-- Creating foreign key on [Id] in table 'Users_Candidate'
ALTER TABLE [dbo].[Users_Candidate]
ADD CONSTRAINT [FK_Candidate_inherits_User]
    FOREIGN KEY ([Id])
    REFERENCES [dbo].[Users]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Id] in table 'Users_Employee'
ALTER TABLE [dbo].[Users_Employee]
ADD CONSTRAINT [FK_Employee_inherits_User]
    FOREIGN KEY ([Id])
    REFERENCES [dbo].[Users]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Id] in table 'Users_HRManager'
ALTER TABLE [dbo].[Users_HRManager]
ADD CONSTRAINT [FK_HRManager_inherits_User]
    FOREIGN KEY ([Id])
    REFERENCES [dbo].[Users]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------